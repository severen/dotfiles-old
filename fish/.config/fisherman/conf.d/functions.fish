function reload
  printf '==> Sourcing ~/.config/fish/config.fish\n'
  source ~/.config/fish/config.fish
end

# TODO: Check if $argv is an argument for mkdir.
function mkd
  mkdir -p $argv
  cd $argv
end

# Make Tmux re-attach to the Tmux session called main if it is running.
function tmux
  if test $argv[1]
    /usr/bin/tmux $argv -f $XDG_CONFIG_HOME/tmux/tmux.conf
  else
    /usr/bin/tmux -f $XDG_CONFIG_HOME/tmux/tmux.conf \
      new-session -A -s main
  end
end
