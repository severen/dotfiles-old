## XDG Base Directory Specification workarounds.
alias ncmpcpp "ncmpcpp -c $XDG_CONFIG_HOME/ncmpcpp/config"

## Editors
alias emacs 'emacs -nw'
alias edit 'eval $EDITOR'

## Git
# Pretty printer for `git log`
alias gpl 'git log --pretty=format:"%Cred%h%Creset -%C(yellow)%d%Creset %s \
%Cgreen(%cr) %C(bold blue)<%an>%Creset" --abbrev-commit'

# Alias the GitHub Git wrapper command to Git if both Git and Hub are in the current
# $PATH.
if which git > /dev/null; and which hub > /dev/null
  alias git hub
end
