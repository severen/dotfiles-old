#
# ~/.bash_profile
#

PATH="$(ruby -e 'print Gem.user_dir')":$PATH
export PATH

export XDG_CONFIG_HOME="$HOME/.config"
export XDG_DATA_HOME="$HOME/.local/share"
export XDG_CACHE_HOME="$HOME/.cache"

## XDG Base Directory Specification workarounds.
# Bash
if [ ! -d "$XDG_DATA_HOME/bash" ]; then
  mkdir "$XDG_DATA_HOME/bash"
fi
export HISTFILE="$XDG_DATA_HOME/bash/history"

# Vim
# export VIMINIT='let $MYVIMRC="$XDG_CONFIG_HOME/vim/vimrc" | source $MYVIMRC'

# Cargo
export CARGO_HOME="$XDG_DATA_HOME/cargo"

# CCache
export CCACHE_DIR="$XDG_CACHE_HOME/ccache"

# GPG
export GNUPGHOME="$XDG_CONFIG_HOME/gnupg"

# HTTPIE
export HTTPIE_CONFIG_DIR="$XDG_CONFIG_HOME/httpie"

# IPython/Jupyter
export IPYTHONDIR="$XDG_CONFIG_HOME/jupyter"
export JUPYTER_CONFIG_DIR="$XDG_CONFIG_HOME/jupyter"

# Tmux
mkdir -p "$XDG_RUNTIME_DIR/tmux"
export TMUX_TMPDIR="$XDG_RUNTIME_DIR/tmux"

# XInit
export XINITRC="$XDG_CONFIG_HOME/X11/xinitrc"
xrdb -load -merge "$HOME/.config/X11/xresources"

# Stack
export STACK_ROOT="$XDG_DATA_HOME/stack"

# Weechat
export WEECHAT_HOME="$XDG_CONFIG_HOME/weechat"

# NVIDIA/CUDA
if [ ! -d "$XDG_CACHE_HOME/nv" ]; then
  mkdir "$XDG_CACHE_HOME/nv"
fi
export __GL_SHADER_DISK_CACHE_PATH="$XDG_CACHE_HOME/nv"
export CUDA_CACHE_PATH="$XDG_CACHE_HOME/nv"

# Wine
export WINEPREFIX="$XDG_DATA_HOME/wine/prefixes/default"

# Tmux
export TMUX_TMPDIR="$XDG_RUNTIME_DIR/tmux"

# Racer
export RUST_SRC_PATH="$HOME/.local/src/rust/src"

[[ -f "$HOME/.bashrc" ]] && . "$HOME/.bashrc"
