#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
source "$HOME/.bash_prompt"

export EDITOR=nvim
export SUDO_EDITOR="$EDITOR"
