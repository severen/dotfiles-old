**This repository is outdated, I now manage my dotfiles with [YADM][yadm].**

*You can find the new repository [here][new-repo].*

My personal configuration files and scripts for my operating system of choice
(Linux (Arch and Fedora)) and the various programs that I make use of.

## Usage
- Install GNU Stow using your distribution's package manager.
- Run `stow config_folder` where `config_folder` is a folder such as `bash` or
  `systemd`.

[yadm]: https://github.com/TheLocehiliosan/yadm
[new-repo]: https://github.com/SShrike/dotfiles
